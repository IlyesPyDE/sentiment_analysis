from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest

class UITest(unittest.TestCase):
    def setUp(self):
        # Utiliser WebDriver pour Chrome
        self.driver = webdriver.Chrome()

    def test_google_search(self):
        driver = self.driver
        driver.get('https://www.google.com')
        self.assertIn("Google", driver.title)
        element = driver.find_element(By.NAME, 'q')
        element.send_keys('GitLab CI/CD')
        element.submit()
        self.assertIn("GitLab CI/CD", driver.page_source)

    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()